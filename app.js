const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const axios = require('axios');

const app = express();

app.use(logger('dev'));
app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static(path.join(__dirname, 'public/index.html')));

const sensors = {
  temperature: {times: [], values: []},
};

app.get('/sensors', (req, res) => {
  res.json(sensors);
});

setInterval(async () => {
  const response = await axios.get('http://192.168.1.26/settings');
  const value = Number(response.data.ambient);
  
  sensors.temperature.times.push(new Date().getTime());
  sensors.temperature.values.push(value);
}, 60000);

module.exports = app;
