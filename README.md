# Sensor Poll

Polls a server for sensor values and stores the history in memory. Currently tracks temperature from a hard-coded LAN address (ESP8266 A/C controller).
