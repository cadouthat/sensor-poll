function getSensorHistory() {
  return $.ajax({
    url: '/sensors',
    method: 'GET',
    dataType: 'json',
  });
}
